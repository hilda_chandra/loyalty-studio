package com.studio.loyalty.services.benefit;

import com.studio.loyalty.dtos.BenefitDto;
import com.studio.loyalty.entities.BenefitEntity;

import java.util.List;

public interface BenefitService {

    List<BenefitEntity> getAll();

    BenefitEntity getOne(String id);

    BenefitEntity save(BenefitDto role) throws Exception;

    BenefitEntity update(BenefitDto role);

    Object delete(String id) throws Exception;

}
