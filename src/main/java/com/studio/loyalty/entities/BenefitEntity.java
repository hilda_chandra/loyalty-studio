package com.studio.loyalty.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "benefits")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BenefitEntity extends BaseEntity {

    @NotEmpty
    private String discount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "benefits_ranks",
            joinColumns = {@JoinColumn(name = "benefit_id")},
            inverseJoinColumns = {@JoinColumn(name = "rank_id")}
    )
    @ToString.Exclude
    @JsonIgnore
    private RankEntity rank;

}
