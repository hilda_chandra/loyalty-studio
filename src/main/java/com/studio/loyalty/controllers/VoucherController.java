package com.studio.loyalty.controllers;

import com.studio.loyalty.dtos.ReedemDto;
import com.studio.loyalty.dtos.VoucherDto;
import com.studio.loyalty.entities.VoucherEntity;
import com.studio.loyalty.services.voucher.VoucherService;
import com.studio.loyalty.utils.ResponseUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/voucher")
@Api(tags = "Voucher")
public class VoucherController {

    @Autowired
    VoucherService voucherService;

    @PostMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> create(@RequestBody @Valid VoucherDto voucher) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", voucherService.save(voucher));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @GetMapping
    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    public ResponseEntity<?> getAll() {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", voucherService.getAll());
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @PutMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> update(@RequestBody @Valid VoucherDto voucher) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", voucherService.update(voucher, voucher.getId()));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @DeleteMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> delete(@RequestBody @Valid VoucherDto voucher) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", voucherService.delete(voucher.getId()));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @PostMapping("/reedem")
    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    public ResponseEntity<?> reedem(@RequestBody @Valid ReedemDto reedem) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", voucherService.reedem(reedem));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }
}
