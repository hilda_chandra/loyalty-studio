package com.studio.loyalty.controllers;

import com.studio.loyalty.dtos.BenefitDto;
import com.studio.loyalty.entities.BenefitEntity;
import com.studio.loyalty.services.benefit.BenefitService;
import com.studio.loyalty.utils.ResponseUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/benefit")
@Api(tags = "Benefit")
public class BenefitController {

    @Autowired
    BenefitService benefitService;

    @PostMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> create(@RequestBody @Valid BenefitDto benefit) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", benefitService.save(benefit));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @GetMapping
    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    public ResponseEntity<?> getAll() {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", benefitService.getAll());
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @PutMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> update(@RequestBody @Valid BenefitDto benefit) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", benefitService.update(benefit));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @DeleteMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> delete(@RequestBody @Valid BenefitDto benefit) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", benefitService.delete(benefit.getId()));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }
}
