package com.studio.loyalty.controllers;

import com.studio.loyalty.dtos.UserDto;
import com.studio.loyalty.entities.UserEntity;
import com.studio.loyalty.services.user.UserService;
import com.studio.loyalty.utils.ResponseUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@Api(tags = "User")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> create(@RequestBody @Valid UserDto user) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", userService.save(user));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @GetMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> getAll() {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", userService.getAll());
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @GetMapping("{id}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> getOne(@PathVariable String id) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", userService.getOne(id));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @GetMapping("/profile/{email}")
    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    public ResponseEntity<?> getEmail(@PathVariable String email) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", userService.getEmail(email));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @PutMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> update(@RequestBody @Valid UserDto user) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", userService.update(user, user.getId()));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }

    @DeleteMapping
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<?> delete(@RequestBody @Valid UserDto user) {
        try {
            return ResponseUtils.generate(HttpStatus.OK, "Success", userService.delete(user.getId()));
        } catch (Exception e) {
            return ResponseUtils.generate(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
    }
}
